Maze in Java using OpenGL fixed pipeline.
Implementation of scene rendering algorithms.
Implementation of algorithm for finding the shortest path and its tracking.
Model parser and model texture mapping.
