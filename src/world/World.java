package world;

import audio.Sound;
import global.AbstractRenderer;
import global.GLCamera;
import gui.Menu;
import models.Light;
import models.Player;
import models.Skybox;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import transforms.Vec3D;

import java.io.IOException;
import java.nio.DoubleBuffer;
import java.util.Locale;

import static global.GluUtils.gluPerspective;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL33.*;

public class World extends AbstractRenderer {

    private float dx, dy, ox, oy;
    private boolean moveW = false, moveS = false, moveA = false, moveD = false, shiftHold = false, hint = false;
    private float zenit, azimut;
    private boolean showMenu = true, gameStarted = false;
    private boolean end = false;

    private long oldmils;
    private long oldFPSmils;
    private double fps;

    private int reso = 1;
    private float brightness = 0.1f;

    private Player player;
    private WorldMap worldMap;
    private Menu menu;
    private Light light;
    private long startTime, stopTime;
    private final Sound sound = new Sound();
    private final Sound footstep = new Sound();
    private final Sound owl = new Sound();

    private float volume = -20f;

    public World() {

        super();

        glfwWindowSizeCallback = new GLFWWindowSizeCallback() {
            @Override
            public void invoke(long window, int w, int h) {
                if (w > 0 && h > 0) {
                    width = w;
                    height = h;

                    System.out.println("Windows resize to [" + w + ", " + h + "]");
                    if (textRenderer != null) {
                        textRenderer.resize(width, height);
                        menu.setTexture(width,0);
                    }
                }
            }
        };

        glfwKeyCallback = new GLFWKeyCallback() {
            @Override
            public void invoke(long window, int key, int scancode, int action, int mods) {

                if (action == GLFW_PRESS) {
                    switch (key) {
                        case 333 ->{
                            volume-=1f;
                            if(volume >=6)
                                volume=5.9f;
                            sound.setVolume(volume);
                            footstep.setVolume(volume);
                            owl.setVolume(volume);
                        }

                        case 334 ->{
                            volume+=1f;
                            if(volume >=6)
                                volume=5.9f;
                            sound.setVolume(volume);
                            footstep.setVolume(volume);
                            owl.setVolume(volume);
                        }

                        case GLFW_KEY_W -> {
                            moveW = true;
                            footstep.playloop();
                        }
                        case GLFW_KEY_S -> {
                            moveS = true;
                            footstep.playloop();
                        }
                        case GLFW_KEY_A -> {
                            moveA = true;
                            footstep.playloop();
                        }
                        case GLFW_KEY_D -> {
                            moveD = true;
                            footstep.playloop();
                        }
                        case GLFW_KEY_LEFT_SHIFT -> shiftHold = true;
                        case GLFW_KEY_P -> {
                            if (player.getScore() < 500) return;

                            player.setScore(player.getScore() - 500);
                            hint = !hint;
                            if (hint) {
                                worldMap.setPathStart(player.getPosition());
                            }
                            worldMap.showShortestPath(hint);
                        }
                        case GLFW_KEY_ESCAPE -> {
                            if (!end) {
                                if (gameStarted) {
                                    showMenu = !showMenu;
                                    if (showMenu) {
                                        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
                                        glfwSetCursorPos(window, width / 2, height / 2);
                                    } else glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
                                }
                            } else glfwSetWindowShouldClose(window, true);
                        }
                    }
                }
                if (action == GLFW_RELEASE) {
                    switch (key) {
                        case GLFW_KEY_W -> moveW = false;
                        case GLFW_KEY_S -> moveS = false;
                        case GLFW_KEY_A -> moveA = false;
                        case GLFW_KEY_D -> moveD = false;
                        case GLFW_KEY_LEFT_SHIFT -> shiftHold = false;
                    }
                }
            }
        };
        glfwMouseButtonCallback = new GLFWMouseButtonCallback() {

            @Override
            public void invoke(long window, int button, int action, int mods) {
                DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                glfwGetCursorPos(window, xBuffer, yBuffer);
                double x = xBuffer.get(0);
                double y = yBuffer.get(0);

                if (showMenu) {
                    if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {
                        if (x > width * 0.09 && x < width * 0.359) {
                            if (y > height * 0.35 && y < height * 0.495) {
                                showMenu = false;
                                gameStarted = true;
                                startTime = System.nanoTime();
                                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
                            } else if (y > height * 0.5 && y < height * 0.65) {
                                glfwSetWindowShouldClose(window, true);
                            }
                        } else if ((x > (width * 0.65)) && (x < (width * 0.9))) {
                            if (y > height * 0.35 && y < height * 0.495) {
                                reso = (reso + 1) % 5;
                                switch (reso) {
                                    case 0 -> glfwSetWindowSize(window, 640, 480);
                                    case 1 -> glfwSetWindowSize(window, 800, 600);
                                    case 2 -> glfwSetWindowSize(window, 1600, 1200);
                                    case 3 -> glfwSetWindowSize(window, 1920, 1080);
                                    case 4 -> glfwSetWindowSize(window, 2048, 1152);

                                }
                            } else if (y > height * 0.5 && y < height * 0.65) {
                                brightness = (brightness + 0.1f) % 1;
                            }
                        }
                    }
                }
            }
        };

        glfwCursorPosCallback = new GLFWCursorPosCallback() {
            @Override
            public void invoke(long window, double x, double y) {

                dx = (float) x - ox;
                dy = (float) y - oy;
                ox = (float) x;
                oy = (float) y;
                if (!showMenu) {
                    zenit -= dy / width * 180;
                    if (zenit > 90) zenit = 90;
                    if (zenit <= -90) zenit = -90;
                    azimut += dx / height * 180;
                    azimut = azimut % 360;
                    player.azimuth(azimut);
                    player.zenith(zenit);
                    dx = 0;
                    dy = 0;
                }
            }
        };
    }

    @Override
    public void init() {
        super.init();
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);


        glEnable(GL_DEPTH_TEST);
        glDisable(GL_CULL_FACE);
        glFrontFace(GL_CW);
        glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_FILL);
        glMatrixMode(GL_MODELVIEW);

        glLoadIdentity();

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        worldMap = new WorldMap();
        worldMap.loadWorldMapFromFile("res/map/mapa.csv");

        player = new Player();
        player.setPosition(worldMap.getStart().add(new Vec3D(0.5, 0.2, 0.5)));
        player.setFirstPerson(true);

        worldMap.setPathStart(player.getPosition());
        worldMap.renderWorldMap();
        worldMap.CreateCollectibles();
        worldMap.renderCollectibles(0.005f);

        player.setWorldMap(worldMap);

        Skybox skybox = new Skybox();
        skybox.renderSkyBox();

        menu = new Menu(width, height);

        light = new Light();

        try {
            sound.setFile(1);
            footstep.setFile(0);
            owl.setFile(2);
            owl.playloop();
            sound.playloop();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void display() {


        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        long mils = System.currentTimeMillis();
        if ((mils - oldFPSmils) > 300) {
            fps = 1000 / (double) (mils - oldmils + 1);
            oldFPSmils = mils;
        }
        String textInfo = String.format(Locale.US, "FPS %3.1f", fps);

        float speed = 2;
        if (shiftHold)
            speed = 3;
        float step = speed * (mils - oldmils) / 1000.0f;
        oldmils = mils;


        glEnable(GL_DEPTH_TEST);


        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(45, width / (float) height, 0.001f, 500.0f);

        if (showMenu) {
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();

            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();

            menu.renderMenu((int) player.getPosition().getX(), (int) player.getPosition().getY());
            glCallList(4);
            textRenderer.setScale(4);
            if (!end) {
                textRenderer.addStr2D((int)(width * 0.65), (int)(height * 0.20), "hlasitost v decib. (+/-)" + (int)volume);
                textRenderer.addStr2D((int) (width * 0.65), (int) (height * 0.34), width + " X " + height);
                textRenderer.addStr2D((int) (width * 0.65), (int) (height * 0.72), (int) (brightness * 10) + "/10");
            } else {

                textRenderer.addStr2D((int) (width * 0.2), (int) (height * 0.2), "Finální score: " + player.getScore());
                textRenderer.addStr2D((int) (width * 0.2), (int) (height * 0.3), "Čas: " + (stopTime - startTime) / 1000000000 + " s");
                textRenderer.addStr2D((int) (width * 0.2), (int) (height * 0.4), "Pro vypnutí ESC");
            }
            textRenderer.setScale(1);
            return;
        }
        if (!showMenu) {
            GLCamera cameraSky = new GLCamera(player.getCamera());
            cameraSky.setPosition(new Vec3D());

            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();


            glPushMatrix();
            player.setMatrix();

            glEnable(GL_LIGHTING);
            glShadeModel(GL_SMOOTH);

            light.setPosition(new float[]{(float) player.getPosition().getX(), (float) player.getPosition().getY() + 10, (float) player.getPosition().getZ(), 1f});

            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, new float[]{brightness, brightness, brightness, 1});
            glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, new float[]{0.9f, 0.9f, 0.9f, 1});
            glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, new float[]{0.9f, 0.9f, 0.9f, 1});

            glCallList(3);
            glPopMatrix();

            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            worldMap.renderCollectibles(step);
            glMatrixMode(GL_MODELVIEW);
            glPopMatrix();

            cameraSky.setMatrix();
            glPushMatrix();
            glCallList(2);
            glPopMatrix();

            textRenderer.addStr2D(3, 40, textInfo);
            textRenderer.addStr2D(3, 60, "score: " + player.getScore());
            textRenderer.addStr2D(3, 80, "Find path (500 score)");
            textRenderer.addStr2D(3, 100, "Time: " + ((System.nanoTime() - startTime) / 1000000000) / 60 + ":" + ((System.nanoTime() - startTime) / 1000000000));

            if (moveW) {
                player.forward(step);
            }
            if (moveS) {
                player.backward(step);
            }
            if (moveD) {
                player.right(step);
            }
            if (moveA) {
                player.left(step);
            }
            if(!moveA && !moveD && !moveS && !moveW) footstep.stop();

            if (Math.round(worldMap.getEnd().getX()) == Math.round(player.getPosition().getX())
                    && Math.round(worldMap.getEnd().getZ()) == Math.round(player.getPosition().getZ())) {
                menu.setTexture(width, 1);
                menu.renderMenu(0, 0);
                end = true;
                showMenu = true;
                stopTime = System.nanoTime();

            }
        }
    }

}
