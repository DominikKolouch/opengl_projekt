package world;

import lwjglutils.OGLTexture2D;
import models.Collectible;
import models.Floor;
import models.MapObject;
import models.Wall;
import pathFinder.PathFinder;
import transforms.Vec3D;

import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.lwjgl.opengl.GL11.*;


public class WorldMap {
    List<Point> shortestPath;
    private MapObject[][] worldMap;
    private char[][] pathMap;
    private final List<Point> floorList = new ArrayList<>();
    private Collectible[][] collectibleList;
    private int rows, cols;


    private Vec3D startPosition, finish, pathStart;

    private OGLTexture2D wallTexture2D;
    private OGLTexture2D floorTexture2D;
    private OGLTexture2D pathFinderFloor2D;

    public WorldMap() {
        try {

            floorTexture2D = new OGLTexture2D("textures/maze_floor.jpg");
            wallTexture2D = new OGLTexture2D("textures/maze_wall.jpg");
            pathFinderFloor2D = new OGLTexture2D("textures/PathFinderFloor.jpg");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void loadWorldMapFromFile(String path) {
        int stringLenght = 0;
        List<String> lines = new ArrayList<>();
        try (BufferedReader bf = new BufferedReader(new FileReader(path))) {
            String line;

            int i = 0;
            while ((line = bf.readLine()) != null) {
                lines.add(line);
                if (line.split(";").length > stringLenght)
                    stringLenght = line.split(";").length;
                ++i;
            }

            rows = i;
            cols = stringLenght;
            worldMap = new MapObject[rows][cols];
            pathMap = new char[rows][cols];
            collectibleList = new Collectible[rows][cols];

        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] line;
        for (int i = 0; i < rows; ++i) {
            line = lines.get(i).split(";");
            for (int j = 0; j < cols; ++j) {
                if (line[j].equals("0")) {
                    worldMap[i][j] = new Wall();
                    worldMap[i][j].setTexture2D(wallTexture2D);
                    pathMap[i][j] = '0';
                } else {
                    worldMap[i][j] = new Floor();
                    worldMap[i][j].setTexture2D(floorTexture2D);
                    if (line[j].equals("s"))
                        startPosition = new Vec3D(i, 0.6d, j);
                    if (line[j].equals("f")) {
                        finish = new Vec3D(i + 0.5f, 0.3d, j + 0.5f);
                        Collectible c = new Collectible(i, j);
                        try {
                            c.bindTexture(new OGLTexture2D("textures/finish.jpg"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        collectibleList[i][j] = c;
                    }
                    pathMap[i][j] = '1';
                    floorList.add(new Point(i, j));

                }
            }
        }

    }

    public void renderWorldMap() {
        glNewList(3, GL_COMPILE);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();

        for (int i = 0; i < rows; ++i)
            for (int j = 0; j < cols; ++j) {
                worldMap[i][j].render(i, j);
            }
        glPopMatrix();
        glEndList();

    }

    public void renderCollectibles(float step) {
        //glNewList(5,GL_COMPILE);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (collectibleList[i][j] != null)
                    collectibleList[i][j].render(step);
            }
        }
        glPopMatrix();
        //glEndList();

    }

    public void showShortestPath(boolean hint) {
        if (hint) {
            PathFinder pathFinder = new PathFinder(pathMap, rows, cols, new Point((int) pathStart.getX(), (int) pathStart.getZ()), new Point((int) finish.getX(), (int) finish.getZ()));
            if (pathFinder.findPath()) {
                shortestPath = pathFinder.getShortestPath();

                shortestPath.forEach(x -> {
                    Floor pathFloor = new Floor();
                    if (hint)
                        pathFloor.setTexture2D(pathFinderFloor2D);
                    else pathFloor.setTexture2D(floorTexture2D);
                    if (worldMap[x.x][x.y].getObjectType() == MapObject.ObjectType.FLOOR)
                        worldMap[x.x][x.y] = pathFloor;
                });
            } else System.out.println("Cesta nenalezena");
        }
        renderWorldMap();
    }

    public Vec3D getStart() {
        return startPosition;
    }

    public Vec3D getEnd() {
        return finish;
    }

    public void setPathStart(Vec3D pathStart) {
        this.pathStart = pathStart;
    }

    public MapObject[][] getWorldMap() {
        return worldMap;
    }


    public void CreateCollectibles() {
        Random rnd = new Random();
        for (int i = 0; i < 40; i++) {
            Point col = floorList.get(rnd.nextInt(0, floorList.size()));
            collectibleList[col.x][col.y] = new Collectible(col.x, col.y);
        }

    }

    public Collectible[][] getCollectibleList() {
        return collectibleList;
    }
}
