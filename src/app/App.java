package app;

import global.LwjglWindow;
import world.World;

public class App {
	public static void main(String[] args) {
		new LwjglWindow(new World());
	}
}
