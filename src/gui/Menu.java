package gui;

import lwjglutils.OGLTexture2D;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class Menu {
    private final List<MenuObject> objectList;
    private OGLTexture2D startMenuTexture2D;
    private OGLTexture2D endMenuTexture2D;

    private final int width;
    private final int height;

    public Menu(int width, int height) {
        this.width = width;
        this.height = height;

        try {

            startMenuTexture2D = new OGLTexture2D("textures/Menu.jpg");
            endMenuTexture2D = new OGLTexture2D("textures/bricks.jpg");

        } catch (IOException e) {
            e.printStackTrace();
        }
        objectList = new ArrayList<>();
        objectList.add(new StartMenu());
        setTexture(width, 0);
    }

    public void renderMenu(int x, int y) {

        glNewList(4, GL_COMPILE);

        objectList.get(0).setTexture2D(startMenuTexture2D);
        objectList.get(0).render(x, y);

        glEndList();
    }

    public void setTexture(int windowWidth, int menuConst) {
        try {
            if (menuConst == 0) {

                if (windowWidth >= 1920) {
                    startMenuTexture2D = new OGLTexture2D("textures/Menu.jpg");
                }
                else if (windowWidth >= 1200) {
                    startMenuTexture2D = new OGLTexture2D("textures/FullHDMenu.jpg");
                }
                else startMenuTexture2D = new OGLTexture2D("textures/HDMenu.jpg");
            } else {
                if (windowWidth >= 1920)
                    startMenuTexture2D = new OGLTexture2D("textures/EndMenu.jpg");
                else if (windowWidth >= 1200)
                    startMenuTexture2D = new OGLTexture2D("textures/FullHDEndMenu.jpg");
                else startMenuTexture2D = new OGLTexture2D("textures/HDEndMenu.jpg");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
