package gui;

import static org.lwjgl.opengl.GL11.*;

public class StartMenu extends MenuObject {

    public void render(int x, int y) {
        glEnable(GL_TEXTURE_2D);
        glDisable(GL_LIGHTING);
        texture2D.bind();
        //v rendereru / loaderu nastavit setTexture()
        glMatrixMode(GL_TEXTURE);
        glLoadIdentity();

        glBegin(GL_QUADS);
        glColor3f(1, 1, 1);

        glTexCoord2f(0, 1);
        glVertex3f(-1, -1, 0.1f);
        glTexCoord2f(0, 0);
        glVertex3f(-1, 1, 0.1f);
        glTexCoord2f(1, 0);
        glVertex3f(1, 1, 0.1f);
        glTexCoord2f(1, 1);
        glVertex3f(1, -1, 0.1f);


        glEnd();
        glDisable(GL_TEXTURE_2D);
    }
}
