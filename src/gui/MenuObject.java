package gui;

import lwjglutils.OGLTexture2D;

public abstract class MenuObject {
    protected OGLTexture2D texture2D;

    public void render(int x, int y) {
    }

    public void setTexture2D(OGLTexture2D texture2D) {
        this.texture2D = texture2D;
    }
}
