package models;

import static org.lwjgl.opengl.GL11.*;

public class Wall extends MapObject {
    private float wallHeight;

    public Wall() {
        objectType = ObjectType.WALL;
    }

    @Override
    public void render(int x, int z) {
        setWallHeight(1);
        glEnable(GL_TEXTURE_2D);
        //glDisable(GL_LIGHTING);
        texture2D.bind();
        //v rendereru / loaderu nastavit setTexture()


        glMatrixMode(GL_TEXTURE);
        glLoadIdentity();


        glBegin(GL_QUADS);

        glNormal3d(0, 1, 0);
        glTexCoord2f(0, 1);
        glVertex3f(x, 0, z + 1);
        glTexCoord2f(0, 0);
        glVertex3f(x, wallHeight, z + 1);
        glTexCoord2f(1, 0);
        glVertex3f(x, wallHeight, z);
        glTexCoord2f(1, 1);
        glVertex3f(x, 0, z);


        glTexCoord2f(0, 0);
        glVertex3f(x, wallHeight, z);
        glTexCoord2f(0, 1);
        glVertex3f(x, 0, z);
        glTexCoord2f(1, 1);
        glVertex3f(x + 1, 0, z);
        glTexCoord2f(1, 0);
        glVertex3f(x + 1, wallHeight, z);


        glTexCoord2f(0, 1);
        glVertex3f(x + 1, 0, z + 1);
        glTexCoord2f(0, 0);
        glVertex3f(x + 1, wallHeight, z + 1);
        glTexCoord2f(1, 0);
        glVertex3f(x + 1, wallHeight, z);
        glTexCoord2f(1, 1);
        glVertex3f(x + 1, 0, z);


        glTexCoord2f(1, 1);
        glVertex3f(x + 1, 0, z + 1);
        glTexCoord2f(1, 0);
        glVertex3f(x + 1, wallHeight, z + 1);
        glTexCoord2f(0, 0);
        glVertex3f(x, wallHeight, z + 1);
        glTexCoord2f(0, 1);
        glVertex3f(x, 0, z + 1);
        glEnd();


        glDisable(GL_TEXTURE_2D);
    }

    public void setWallHeight(float wallHeight) {
        this.wallHeight = wallHeight;
    }
}
