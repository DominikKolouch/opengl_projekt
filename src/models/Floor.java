package models;

import static org.lwjgl.opengl.GL11.*;

public class Floor extends MapObject {

    public Floor() {
        objectType = ObjectType.FLOOR;
    }

    @Override
    public void render(int x, int z) {
        glEnable(GL_TEXTURE_2D);
        //glDisable(GL_LIGHTING);
        texture2D.bind();
        //v rendereru / loaderu nastavit setTexture()
        glMatrixMode(GL_TEXTURE);
        glLoadIdentity();

        glBegin(GL_QUADS);

        glColor3f(1, 1, 1);
        glNormal3d(0, 1, 0);


        glTexCoord2f(0, 1);
        glVertex3f(x, 0, z);
        glTexCoord2f(0, 0);
        glVertex3f(x + 1, 0, z);
        glTexCoord2f(1, 0);
        glVertex3f(x + 1, 0, z + 1);
        glTexCoord2f(1, 1);
        glVertex3f(x, 0, z + 1);

        glEnd();
        glDisable(GL_TEXTURE_2D);
    }
}
