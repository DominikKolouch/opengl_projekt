package models;

import global.GLCamera;
import transforms.Vec3D;
import world.WorldMap;


public class Player {
    private final GLCamera camera;
    private WorldMap worldMap;
    private MapObject[][] worldMapObject;
    private Collectible[][] collectibleMap;
    private final double offset = 0.1;
    private double score = 0;

    public Player() {
        camera = new GLCamera();

    }
    //dopsat funkci normalized pro vektor

    public void setFirstPerson(boolean firstPerson) {
        camera.setFirstPerson(firstPerson);
    }

    public void forward(double step) {
        Vec3D helpVec = camera.getPosition().add(
                new Vec3D(checkCollisionX(Math.sin(camera.getAzimuth()) * step),
                        0,
                        checkCollisionZ(-Math.cos(camera.getAzimuth()) * step))
        );
        if (worldMapObject[(int) helpVec.getX()][(int) helpVec.getZ()].getObjectType() != MapObject.ObjectType.WALL) {
            camera.setPosition(camera.getPosition().add(
                    new Vec3D(checkCollisionX(Math.sin(camera.getAzimuth()) * step),
                            0,
                            checkCollisionZ(-Math.cos(camera.getAzimuth()) * step))
            ));
        }
        if ((collectibleMap[(int) helpVec.getX()][(int) helpVec.getZ()] != null) && collectibleMap[(int) helpVec.getX()][(int) helpVec.getZ()].isActive()) {
            collectibleMap[(int) helpVec.getX()][(int) helpVec.getZ()].onCollision();
            score += 100;
            worldMap.renderCollectibles(0.005f);
            System.out.println(score);
        }

    }

    public void backward(double step) {
        forward(-step);
    }

    public void right(double step) {
        Vec3D helpVec = camera.getPosition().add(
                new Vec3D(checkCollisionX(-Math.sin((camera.getAzimuth() - Math.PI / 2)) * step),
                        0,
                        checkCollisionZ(+Math.cos((camera.getAzimuth() - Math.PI / 2)) * step)));

        if (worldMapObject[(int) helpVec.getX()][(int) helpVec.getZ()].getObjectType() != MapObject.ObjectType.WALL) {
            camera.setPosition(camera.getPosition().add(
                    new Vec3D(checkCollisionX(-Math.sin((camera.getAzimuth() - Math.PI / 2)) * step),
                            0,
                            checkCollisionZ(+Math.cos((camera.getAzimuth() - Math.PI / 2)) * step))));
        }
        if ((collectibleMap[(int) helpVec.getX()][(int) helpVec.getZ()] != null) && collectibleMap[(int) helpVec.getX()][(int) helpVec.getZ()].isActive()) {
            collectibleMap[(int) helpVec.getX()][(int) helpVec.getZ()].onCollision();
            score += 100;
            worldMap.renderCollectibles(0.005f);
            System.out.println(score);
        }
    }

    public void left(double step) {
        right(-step);
    }

    public void azimuth(float azimut) {
        camera.setAzimuth(Math.toRadians(azimut));
    }

    public void zenith(float zenit) {
        camera.setZenith(Math.toRadians(zenit));
    }

    public void setMatrix() {
        camera.setMatrix();
    }

    public Vec3D getPosition() {
        return camera.getPosition();
    }

    public void setPosition(Vec3D position) {
        camera.setPosition(position);
    }

    public GLCamera getCamera() {
        return camera;
    }

    public void setWorldMap(WorldMap worldMap) {
        this.worldMap = worldMap;
        worldMapObject = worldMap.getWorldMap();
        collectibleMap = worldMap.getCollectibleList();
    }

    private double checkCollisionX(double step) {

        double playerX = camera.getPosition().getX();
        double playerZ = camera.getPosition().getZ();

        if (step > 0) {
            if ((int) playerX < (int) (playerX + step + offset)) {
                if (worldMapObject[(int) (playerX + step + offset)][(int) playerZ].getObjectType() == MapObject.ObjectType.WALL) {
                    return 0;
                }
                if (worldMapObject[(int) (playerX + step + offset)][(int) (playerZ + offset)].getObjectType() == MapObject.ObjectType.WALL) {
                    return 0;
                }
                if (worldMapObject[(int) (playerX + step + offset)][(int) (playerZ - offset)].getObjectType() == MapObject.ObjectType.WALL) {
                    return 0;
                }
            }
        } else {
            if ((int) playerX + 1 > (int) (playerX + step - offset)) {
                if (worldMapObject[(int) (playerX + step - offset)][(int) playerZ].getObjectType() == MapObject.ObjectType.WALL) {
                    return 0;
                }
                if (worldMapObject[(int) (playerX + step - offset)][(int) (playerZ + offset)].getObjectType() == MapObject.ObjectType.WALL) {
                    return 0;
                }
                if (worldMapObject[(int) (playerX + step - offset)][(int) (playerZ - offset)].getObjectType() == MapObject.ObjectType.WALL) {
                    return 0;
                }
            }
        }
        return step;
    }

    private double checkCollisionZ(double step) {
        double playerX = camera.getPosition().getX();
        double playerZ = camera.getPosition().getZ();

        if (step > 0) {
            if ((int) playerZ < (int) (playerZ + step + offset)) {
                if (worldMapObject[(int) (playerX)][(int) (playerZ + step + offset)].getObjectType() == MapObject.ObjectType.WALL) {
                    return 0;
                }
                if (worldMapObject[(int) (playerX + offset)][(int) (playerZ + step + offset)].getObjectType() == MapObject.ObjectType.WALL) {
                    return 0;
                }
                if (worldMapObject[(int) (playerX - offset)][(int) (playerZ + step + offset)].getObjectType() == MapObject.ObjectType.WALL) {
                    return 0;
                }
            }
        } else {
            if ((int) playerZ + 1 > (int) (playerZ + step - offset)) {
                if (worldMapObject[(int) (playerX)][(int) (playerZ + step - offset)].getObjectType() == MapObject.ObjectType.WALL) {
                    return 0;
                }
                if (worldMapObject[(int) (playerX - offset)][(int) (playerZ + step + offset)].getObjectType() == MapObject.ObjectType.WALL) {
                    return 0;
                }
                if (worldMapObject[(int) (playerX + offset)][(int) (playerZ + step + offset)].getObjectType() == MapObject.ObjectType.WALL) {
                    return 0;
                }
            }
        }
        return step;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
