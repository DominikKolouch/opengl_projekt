package models;

import lwjglutils.OGLModelOBJ;
import lwjglutils.OGLTexture2D;

import java.io.IOException;
import java.nio.FloatBuffer;

import static global.GlutUtils.glutSolidSphere;
import static org.lwjgl.opengl.ARBInternalformatQuery2.GL_TEXTURE_2D;
import static org.lwjgl.opengl.ARBVertexArrayObject.glBindVertexArray;
import static org.lwjgl.opengl.ARBVertexArrayObject.glGenVertexArrays;
import static org.lwjgl.opengl.GL33.*;

public class Collectible {
    boolean minus = false, plus = true;
    private boolean isActive;
    private OGLTexture2D texture;
    private float angle = 0;
    private final float translate = 0.2f;
    private float offset = 0.01f;
    private final int x;
    private final int y;
    private OGLModelOBJ model = new OGLModelOBJ("/obj/coinC2.obj");

    {
        try {
            texture = new OGLTexture2D("textures/SphereTexture.jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Collectible(int x, int y) {
        isActive = true;
        this.x = x;
        this.y = y;
    }

    public void onCollision() {
        if (isActive)
            isActive = false;
    }

    public boolean isActive() {
        return isActive;
    }

    public void render(float step) {
        if (!isActive) return;

        glEnable(GL_LIGHTING);
        glEnable(GL_TEXTURE_2D);
        glColor3f(0, 0, 0);
        texture.bind();
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();

        if (offset >= 0.4f) {
            minus = true;
            plus = false;
        }
        if (offset <= 0f) {
            plus = true;
            minus = false;
        }

        glTranslatef(x + 0.5f, translate + offset, y + 0.5f);
        glRotatef(angle % 360, 0, 1, 0);
        glutSolidSphere(0.2, 16, 16);

        ++angle;

        if (plus) offset += step/10;
        if (minus) offset -= step/10;
        glPopMatrix();
    }

    ///funguje, ale seká se to
    /*
    public void render(float step) {


        int vboId, vaoIdOBJ;

        if (!isActive) return;
        vaoIdOBJ = glGenVertexArrays();
        glBindVertexArray(vaoIdOBJ);
        FloatBuffer fb = model.getVerticesBuffer();
        if (fb != null) {
            vboId = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, vboId);
            fb.rewind();
            glBufferData(GL_ARRAY_BUFFER, fb, GL_STATIC_DRAW);
            glVertexPointer(4, GL_FLOAT, 4 * 4, 0);
        }
        fb = model.getNormalsBuffer();
        if (fb != null) {
            vboId = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, vboId);
            fb.rewind();
            glBufferData(GL_ARRAY_BUFFER, fb, GL_STATIC_DRAW);
            glColorPointer(3, GL_FLOAT, 3 * 4, 0);
            glNormalPointer(GL_FLOAT, 3 * 4, 0);
        }
        fb = model.getTexCoordsBuffer();
        if (fb != null) {
            vboId = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, vboId);
            fb.rewind();
            glBufferData(GL_ARRAY_BUFFER, fb, GL_STATIC_DRAW);
            glTexCoordPointer(2, GL_FLOAT, 2 * 4, 0);
        }

            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
            glBindVertexArray(vaoIdOBJ);
            glEnable(GL_TEXTURE_2D);
            texture.bind();
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);
            glEnableClientState(GL_VERTEX_ARRAY);
            glEnableClientState(GL_NORMAL_ARRAY);
            glEnable(GL_NORMALIZE);
            glEnable(GL_LIGHTING);
            glMatrixMode(GL_MODELVIEW);
        glPushMatrix();

        if (offset >= 0.4f) {
            minus = true;
            plus = false;
        }
        if (offset <= 0f) {
            plus = true;
            minus = false;
        }

        glTranslatef(x + 0.5f, translate + offset, y + 0.5f);
        glRotatef(angle % 360, 0, 1, 0);
        glDrawArrays(GL_TRIANGLES, 0, model.getVerticesBuffer().limit());
        ++angle;

        if (plus) offset += step/10;
        if (minus) offset -= step/10;
        glPopMatrix();
            glDisableClientState(GL_COLOR_ARRAY);
            glDisableClientState(GL_VERTEX_ARRAY);
            glDisableClientState(GL_NORMAL_ARRAY);
            glDisableClientState(GL_TEXTURE_COORD_ARRAY);
            glDisable(GL_TEXTURE_2D);
            glBindVertexArray(0);
            glDisable(GL_VERTEX_ARRAY);
            glDisable(GL_COLOR_ARRAY);
            glDisable(GL_TEXTURE_COORD_ARRAY);
            glDisableClientState(GL_COLOR_ARRAY);
            glDisableClientState(GL_VERTEX_ARRAY);

    }*/

    public void bindTexture(OGLTexture2D texture2D) {
        this.texture = texture2D;
    }
}
