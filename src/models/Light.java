package models;

import static org.lwjgl.opengl.GL33.*;

public class Light {
    private final float r = 0.3f;
    private final float g = .3f;
    private final float b = .3f;
    private float[] position;

    public Light() {
        glEnable(GL_LIGHT0);
        glLightfv(GL_LIGHT0, GL_AMBIENT, new float[]{0, 0, 0, 1});
        glLightfv(GL_LIGHT0, GL_DIFFUSE, new float[]{r, g, b, 1});
        glLightfv(GL_LIGHT0, GL_SPECULAR, new float[]{0, 0, 0, 1});
    }

    public float[] getPosition() {
        return position;
    }

    public void setPosition(float[] position) {
        this.position = position;
        glLightfv(GL_LIGHT0, GL_POSITION, position);
    }

}
