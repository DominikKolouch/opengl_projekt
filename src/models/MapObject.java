package models;

import lwjglutils.OGLTexture2D;

public abstract class MapObject {
    protected OGLTexture2D texture2D;
    protected ObjectType objectType;
    protected float res = 0.1f;

    public void render(int x, int y) {
    }

    public OGLTexture2D getTexture2D() {
        return texture2D;
    }

    public void setTexture2D(OGLTexture2D texture2D) {
        this.texture2D = texture2D;
    }

    public ObjectType getObjectType() {
        return objectType;
    }

    public enum ObjectType {
        WALL, FLOOR
    }
}
