package pathFinder;

import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class PathFinder {

    char[][] charMap;
    int[][] shadowMap;
    int rows, cols;
    Point start, end;
    List<Point> shortestPath;

    public PathFinder(char[][] charMap, int rows, int cols, Point start, Point end) {
        this.charMap = charMap;
        this.rows = rows;
        this.cols = cols;
        this.start = start;
        this.end = end;
        shadowMap = new int[rows][cols];
        shortestPath = new ArrayList<>();
    }


    public boolean isValid(int row, int col) {
        return (row >= 0) && (row < rows) &&
                (col >= 0) && (col < cols);
    }

    public boolean findPath() {
        int[] rowNum = {-1, 0, 0, 1};
        int[] colNum = {0, -1, 1, 0};

        if ((charMap[(int) start.getX()][(int) start.getY()] != '1') ||
                (charMap[(int) end.getX()][(int) end.getY()] != '1')) {
            return false;
        }

        boolean[][] visited = new boolean[rows][cols];

        visited[(int) start.getX()][(int) start.getY()] = true;

        Queue<queueNode> q = new LinkedList<>();

        queueNode s = new queueNode(new Point((int) start.getX(), (int) start.getY()), 0);
        q.add(s);

        while (!q.isEmpty()) {
            queueNode curr = q.peek();
            Point pt = curr.pt;

            if (pt.x == (int) end.getX() && pt.y == (int) end.getY()) {

                while (curr.pt.x != curr.parrent.pt.x || curr.pt.y != curr.parrent.pt.y) {
                    shortestPath.add(curr.pt);
                    curr = curr.parrent;
                }
                return true;
            }

            q.remove();

            for (int i = 0; i < 4; i++) {
                int row = pt.x + rowNum[i];
                int col = pt.y + colNum[i];

                if (isValid(row, col) &&
                        charMap[row][col] == '1' &&
                        !visited[row][col]) {
                    visited[row][col] = true;
                    queueNode Adjcell = new queueNode(new Point(row, col), curr.dist + 1, curr);
                    q.add(Adjcell);
                }
            }
        }
        return false;
    }

    public List<Point> getShortestPath() {
        return shortestPath;
    }
}


class queueNode {
    Point pt;
    int dist;
    queueNode parrent;

    public queueNode(Point pt, int dist, queueNode parrent) {
        this.pt = pt;
        this.dist = dist;
        this.parrent = parrent;
    }

    public queueNode(Point pt, int dist) {
        this.pt = pt;
        this.dist = dist;
        parrent = this;
    }
}
