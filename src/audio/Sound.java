package audio;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;

public class Sound {

    Clip clip;
    String sourceURL[] = new String[30];

    public Sound()
    {
        sourceURL[0] = ("res/sounds/footstep.wav");
        sourceURL[1] = ("res/sounds/gameSound.wav");
        sourceURL[2] = ("res/sounds/owl.wav");
    }

    public void setFile(int index) throws IOException {
        try {
            AudioInputStream stream = AudioSystem.getAudioInputStream(new File(sourceURL[index]));

            clip = AudioSystem.getClip();
            clip.open(stream);
        } catch (UnsupportedAudioFileException | LineUnavailableException e) {
            e.printStackTrace();
        }

    }
    public void playloop()
    {
        clip.start();
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void stop()
    {
        clip.stop();
    }


    public void setVolume(float volume)
    {
        FloatControl gainControl =
                (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
        gainControl.setValue(volume);
    }
}
